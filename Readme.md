Activiti rest connector
=========

This library is for connection to activiti server by REST service.



Version
----
0.2


API functions
----
```java
    /**
     * Get the lates process.
     *
     * @param processDefinition Process definition key.
     * @return Latest process definition. If not exist return null.
     */
    public ProcessDefinition getLatestProcessDefinition(String processDefinition);


    /**
     * Create process by processDefinition and properties.
     *
     * @param processDefinition String value of process definition key.
     * @param properties        List of properties.
     */
    public void createProcess(String processDefinition, List<ActivitiProperty> properties);

    /** Get task by process definition and process instance id.
     * @param processDefinition Process definition key.
     * @param processInstanceId Process instance id.
     * @return If task not exist return null, else return task.
     */
    public ProcessInstance getTask(String processDefinition, String processInstanceId);

    /**
     * Update process task by taskId and set up properties.
     * @param taskId  Task of process.
     * @param properties Properties to setup.
     */
    public void confirmTask(String taskId, List<ActivitiProperty> properties);

    /**
     * Claim task by taskId to user.
     * @param taskId TaskId of task.
     * @param userId UserId to claimTask.
     */
    public void claimTask(String taskId, String userId);

```

Create process example
----
```java
List<ActivitiProperty> properties = new ArrayList<ActivitiProperty>();
        properties.add(getProperty("titles_pre", externRegistrationUser.getTitleAfter()));
        properties.add(getProperty("titles_post", externRegistrationUser.getTitleBefore()));
        properties.add(getProperty("first_name", externRegistrationUser.getFirstName()));
        properties.add(getProperty("last_name", externRegistrationUser.getSurName()));
        properties.add(getProperty("dateBirth", externRegistrationUser.getDateBirth()));
        properties.add(getProperty("company", externRegistrationUser.getCompany()));
        properties.add(getProperty("email", externRegistrationUser.getEmail()));
        properties.add(getProperty("phone", externRegistrationUser.getPhone()));
        setJSONProperty(properties, "required_roles", externRegistrationUser.getRequiredRoles());
        properties.add(getProperty("referencePerson", externRegistrationUser.getReferencePerson()));
        properties.add(getProperty("referenceDepartments", externRegistrationUser.getReferenceDepartment()));

        properties.add(getProperty("note", externRegistrationUser.getNote()));

        activitiService.createProcess(externProcessDefId, properties);
```

Configuration
--------------
Example configuration in spring. This configuration use basic auth.

``` xml
   <bean id="activitiRestTemplate" class="org.springframework.web.client.RestTemplate">
        <property name="errorHandler" ref="activitiErrorHandler" />
        <property name="interceptors">
            <list>
                <bean id="basicAuthInterceptor" class="cz.cvut.fit.portal.activiti.interceptor.BasicAuthInterceptor">
                    <constructor-arg name="username" value="${activiti.username}" />
                    <constructor-arg name="password" value="${activiti.password}" />
                </bean>
            </list>
        </property>
        <property name="messageConverters">
            <list>
                <bean id="stringMessageConverter" class="org.springframework.http.converter.StringHttpMessageConverter" />
                <bean id="jsonMessageConverter" class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
                    <property name="supportedMediaTypes" value="application/json" />
                </bean>
            </list>
        </property>
    </bean>
    <bean id="activitiErrorHandler" class="cz.cvut.fit.portal.activiti.handler.ActivitiErrorHandler" ></bean>
```
Properties of configuration
----
```properties
activiti.rest.base=http://activiti.vpn/rest/
activiti.username=user
activiti.password=userpassword
activiti.rest.process.definition.latest=service/repository/process-definitions?key={process_def}&latest=true
activiti.rest.process.instance=service/runtime/tasks?includeProcessVariables=true&processDefinitionKey={process_def}&processInstanceId={process_instance_id}
activiti.rest.process=service/form/form-data
```
License
----
MIT


Authors
----
<mailto:vladimir.mezera@gmail.com>
