package cz.cvut.fit.portal.activiti.service.impl;

import cz.cvut.fit.portal.activiti.model.*;
import cz.cvut.fit.portal.activiti.service.ActivitiService;
import cz.cvut.fit.portal.activiti.utils.UriBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import java.util.Arrays;
import java.util.List;


@Service
public class ActivitiServiceImpl implements ActivitiService {

    private
    @Value("${activiti.rest.base}")
    String activitiUrl;
    private
    @Value("${activiti.rest.process.definition.latest}")
    String processLatest;
    private
    @Value("${activiti.rest.process.instance}")
    String processInstance;

    @Value("${activiti.rest.tasks}")
    private String tasksUrl;

    private
    @Value("${activiti.rest.process}")
    String processCreate;

    private static final String PROCESS_DEF = "process_def";
    private static final String USERNAME = "username";
    private static final String PROCESS_INSTANCE_ID = "process_instance_id";

    @Autowired
    private RestOperations activitiRestTemplate;


    public ProcessDefinition getLatestProcessDefinition(String processDefinition) {
        UriBuilder uriBuilder =
                UriBuilder.fromBase(activitiUrl).path(processLatest).expand(PROCESS_DEF, processDefinition);

        ResponseEntity<ActivitiProcessDefResponse> def =
                activitiRestTemplate.getForEntity(uriBuilder.build(), ActivitiProcessDefResponse.class);
        ActivitiProcessDefResponse resp = def.getBody();
        if (resp.getData().length > 0) {
            return resp.getData()[0];
        }
        return null;


    }

    public void createProcess(String processDefinition, List<ActivitiProperty> properties) {
        UriBuilder uriBuilder = UriBuilder.fromBase(activitiUrl).path(processCreate);
        ActivitiProcessRequest request = new ActivitiProcessRequest();
        request.setProcessDefinitionId(processDefinition);
        request.setProperties(properties);
        activitiRestTemplate.postForObject(uriBuilder.build(), request, String.class);
    }

    public ProcessInstance getTask(String processDefinition, String processInstanceId) {
        UriBuilder uriBuilder =
                UriBuilder.fromBase(activitiUrl).path(processInstance).expand(PROCESS_DEF, processDefinition)
                        .expand(PROCESS_INSTANCE_ID, processInstanceId);
        ActivitiProcessInstanceResponse response =
                activitiRestTemplate.getForObject(uriBuilder.build(), ActivitiProcessInstanceResponse.class);
        if (response.getData() != null && response.getData().length > 0) {
            return response.getData()[0];
        }
        return null;
    }

    @Override
    public void confirmTask(String taskId, List<ActivitiProperty> properties) {
        UriBuilder uriBuilder = UriBuilder.fromBase(activitiUrl).path(processCreate);
        ActivitiProcessRequest request = new ActivitiProcessRequest();
        request.setTaskId(taskId);
        request.setProperties(properties);
        activitiRestTemplate.postForObject(uriBuilder.build(), request, String.class);
    }

    @Override
    public void claimTask(String taskId, String userId) {
        throw new UnsupportedOperationException("This operation is not supported.");
    }

    @Override
    public List<ActivitiTask> findActiveTasks(String username) {
        UriBuilder uriBuilder =
                UriBuilder.fromBase(activitiUrl).path(tasksUrl).expand(USERNAME, username);

        ActivitiTasksResponse response =
                activitiRestTemplate.getForObject(uriBuilder.build(), ActivitiTasksResponse.class);
        final ActivitiTask[] data = response.getData();
        return Arrays.asList(data);
    }

}
