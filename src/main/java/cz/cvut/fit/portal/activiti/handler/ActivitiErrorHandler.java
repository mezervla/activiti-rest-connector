package cz.cvut.fit.portal.activiti.handler;

import cz.cvut.fit.portal.activiti.exception.ActivitiConnectorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

public class ActivitiErrorHandler implements ResponseErrorHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ActivitiErrorHandler.class);

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {

        LOG.error("Response error: {} {}", response.getStatusCode(), response.getStatusText());
        throw new ActivitiConnectorException(String.format("Response error: %s %s", response.getStatusCode(),
                response.getStatusText()));
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        HttpStatus.Series series = response.getStatusCode().series();
        return (HttpStatus.Series.CLIENT_ERROR.equals(series) || HttpStatus.Series.SERVER_ERROR.equals(series));
    }

}
