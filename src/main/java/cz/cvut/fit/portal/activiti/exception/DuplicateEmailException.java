package cz.cvut.fit.portal.activiti.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.CONFLICT, reason="Duplicated email")
public class DuplicateEmailException extends RuntimeException {
}
