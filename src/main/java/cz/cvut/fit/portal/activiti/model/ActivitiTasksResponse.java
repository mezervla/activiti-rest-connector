package cz.cvut.fit.portal.activiti.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivitiTasksResponse {

    private ActivitiTask[] data;

    public ActivitiTask[] getData() {
        return data;
    }

    public void setData(ActivitiTask[] data) {
        this.data = data;
    }
}
