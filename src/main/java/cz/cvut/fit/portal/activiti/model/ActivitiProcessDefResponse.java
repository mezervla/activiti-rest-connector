package cz.cvut.fit.portal.activiti.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivitiProcessDefResponse {

    private ProcessDefinition[] data;

    public ProcessDefinition[] getData() {
        return data;
    }

    public void setData(ProcessDefinition[] data) {
        this.data = data;
    }
}
