package cz.cvut.fit.portal.activiti.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import java.util.ArrayList;
import java.util.List;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivitiProcessRequest {

    private String processDefinitionId;

    private String taskId;

    private List<ActivitiProperty> properties = new ArrayList<ActivitiProperty>();

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public List<ActivitiProperty> getProperties() {
        return properties;
    }

    public void setProperties(List<ActivitiProperty> properties) {
        this.properties = properties;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
