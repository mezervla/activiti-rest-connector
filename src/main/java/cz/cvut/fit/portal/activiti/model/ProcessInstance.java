package cz.cvut.fit.portal.activiti.model;

import java.util.List;

public class ProcessInstance extends ProcessDefinition {

    private List<ActivitiProperty> variables;


    public List<ActivitiProperty> getVariables() {
        return variables;
    }

    public void setVariables(List<ActivitiProperty> variables) {
        this.variables = variables;
    }
}
