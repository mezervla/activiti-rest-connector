package cz.cvut.fit.portal.activiti.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.UNAUTHORIZED, reason="Unauthorized access.")
public class AuthException extends RuntimeException {
}
