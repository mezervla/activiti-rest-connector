package cz.cvut.fit.portal.activiti.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;


@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Unauthorized access.")
public class ActivitiConnectorException extends IOException {
    public ActivitiConnectorException(String s) {
        super(s);
    }

}
