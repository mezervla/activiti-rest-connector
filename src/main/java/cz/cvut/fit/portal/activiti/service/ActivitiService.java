package cz.cvut.fit.portal.activiti.service;

import cz.cvut.fit.portal.activiti.model.ActivitiProperty;
import cz.cvut.fit.portal.activiti.model.ActivitiTask;
import cz.cvut.fit.portal.activiti.model.ProcessDefinition;
import cz.cvut.fit.portal.activiti.model.ProcessInstance;

import java.util.List;


/**
 * Interface for communication with activiti server.
 */
public interface ActivitiService {

    /**
     * Get the lates process.
     *
     * @param processDefinition Process definition key.
     * @return Latest process definition. If not exist return null.
     */
    ProcessDefinition getLatestProcessDefinition(String processDefinition);


    /**
     * Create process by processDefinition and properties.
     *
     * @param processDefinition String value of process definition key.
     * @param properties        List of properties.
     */
    void createProcess(String processDefinition, List<ActivitiProperty> properties);

    /** Get task by process definition and process instance id.
     * @param processDefinition Process definition key.
     * @param processInstanceId Process instance id.
     * @return If task not exist return null, else return task.
     */
    ProcessInstance getTask(String processDefinition, String processInstanceId);

    /**
     * Update process task by taskId and set up properties.
     * @param taskId  Task of process.
     * @param properties Properties to setup.
     */
    void confirmTask(String taskId, List<ActivitiProperty> properties);

    /**
     * Claim task by taskId to user.
     * @param taskId TaskId of task.
     * @param userId UserId to claimTask.
     */
    void claimTask(String taskId, String userId);

    List<ActivitiTask> findActiveTasks(String username);

}
