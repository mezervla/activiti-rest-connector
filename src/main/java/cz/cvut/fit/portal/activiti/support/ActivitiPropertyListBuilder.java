package cz.cvut.fit.portal.activiti.support;

import cz.cvut.fit.portal.activiti.model.ActivitiProperty;

import java.util.ArrayList;
import java.util.List;

public class ActivitiPropertyListBuilder {

    private final List<ActivitiProperty> properties = new ArrayList<>();


    public ActivitiPropertyListBuilder add(String id, Object value) {
        properties.add(new ActivitiProperty(id, value));
        return this;
    }

    public List<ActivitiProperty> toList() {
        return properties;
    }
}
