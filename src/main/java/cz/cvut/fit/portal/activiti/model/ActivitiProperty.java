package cz.cvut.fit.portal.activiti.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivitiProperty {

    private String id;
    private String name;
    private Object value;

    public ActivitiProperty() {

    }

    public ActivitiProperty(String id, Object value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
