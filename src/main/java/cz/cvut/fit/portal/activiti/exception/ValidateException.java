package cz.cvut.fit.portal.activiti.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.UNPROCESSABLE_ENTITY, reason="Validation error")
public class ValidateException extends RuntimeException {
}
