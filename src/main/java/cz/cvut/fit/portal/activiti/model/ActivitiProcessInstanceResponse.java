package cz.cvut.fit.portal.activiti.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivitiProcessInstanceResponse {

    private ProcessInstance[] data;

    public ProcessInstance[] getData() {
        return data;
    }

    public void setData(ProcessInstance[] data) {
        this.data = data;
    }
}
